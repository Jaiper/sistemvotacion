package Entidad;


import java.time.LocalDateTime;

public class Notificacion {

    private String nombreDpto;

    private String nombreMunicipio;

    private LocalDateTime fechaVotacion;
    
    private Persona persona;

    public Notificacion() {
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public LocalDateTime getFechaVotacion() {
        return fechaVotacion;
    }

    public void setFechaVotacion(LocalDateTime fechaVotacion) {
        this.fechaVotacion = fechaVotacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    
    
}
